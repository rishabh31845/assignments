//This js is used to implement calculator
var num1,num2,operator,totalNumber,answer;

//This function will be called on equals key and different operations will be called
function equals(){
if(operator == "+"){
  answer = add();
  document.getElementById('display').value = answer;
}else if (operator == "-") {
  answer = subtract();
  document.getElementById('display').value = answer;
}else if (operator == "*") {
  answer = multiply();
  document.getElementById('display').value = answer;
}else if (operator == "/") {
  answer = division();
  document.getElementById('display').value = answer;
}else if (operator == "%") {
  answer = percentage();
  document.getElementById('display').value = answer;
}
}

//This function will used to display values in calculator
function getDisplay(num){
document.getElementById('display').value+= num;
totalNumber = document.getElementById('display').value;
operator = checkOperator();
num1 = totalNumber.split(operator)[0];
num2 = totalNumber.split(operator)[1];
}

//This function will clear the display
function clearDisplay(){
  document.getElementById('display').value="";
}

//This function will remove last element of display
function backSpace(){
  totalNumber = document.getElementById('display').value;
  var totalNumber_length = totalNumber.length-1;
  var newtotalNumber = totalNumber.substring(0,totalNumber_length);
  document.getElementById('display').value = newtotalNumber;
}

//This function will calculate percentage
function percentage(){
var result = (parseFloat(num1)*parseFloat(num2))/100;
return result.toFixed(2);
}

//This function will calculate addition
function add(){
var result = parseFloat(num1)+parseFloat(num2);
return result.toFixed(2);
}

//This function will calculate subtract
function subtract(){
  var result = parseFloat(num1)-parseFloat(num2);
  return result.toFixed(2);
}

//This function will calculate multiplication
function multiply(){
  var result = parseFloat(num1)*parseFloat(num2);
  return result.toFixed(2);
}

//This function will calculate division
function division(){
  var result = parseFloat(num1)/parseFloat(num2);
  return result.toFixed(2);
}

//This function will check which operator has been called
function checkOperator(){
if(totalNumber.indexOf("+") > 0){
  operator = "+";
  return operator;
}else if (totalNumber.indexOf("-") > 0) {
  operator = "-";
  return operator;
}else if (totalNumber.indexOf("*") > 0) {
  operator = "*";
  return operator;
}else if (totalNumber.indexOf("/") > 0) {
  operator = "/";
  return operator;
}else if(totalNumber.indexOf("%") > 0){
  operator = "%";
  return operator;
}else {
  //
}

}
