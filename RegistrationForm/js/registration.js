
function validateForm()
{
var firstName = firstnameValidate();
var lastName = lastnameValidate();
var phoneNumber = phoneValidate();
var dateBirth = dobValidate();
var emailId = emailValidate();
var password = passValidate();
var confirmPassword = passValidate();
var checkBox = checkboxValidate();

if((firstName && lastName&&phoneNumber&&dateBirth&&emailId&&password&&confirmPassword&&checkBox)==true ){
  alert("Successfull registration done");
}
}
function firstnameValidate(){
  var firstName = document.getElementById('firstName').value;
  if(firstName==""){
    alert("Firstname cannot be empty");
  }else{
    return true;
  }
}
function lastnameValidate(){
  var lastName = document.getElementById('lastName').value;
  if(lastName==""){
    alert("Lastname cannot be empty");
  }else{
    return true;
  }
}
function phoneValidate(){
  var phoneNumber = document.getElementById('phoneNo').value;
  if(phoneNumber.length!=10 || phoneNumber==""){
    alert("Phone number minimum length 10");
  }else{
    return true;
  }
}
function dobValidate(){
  var dateBirth = document.getElementById('dob').value;
  if(dateBirth==""){
    alert("Select date of birth");
  }else {
    return true;
  }
}

function emailValidate(){
  var emailId = document.getElementById('email').value;
  var atpos,dotpos;
  atpos = emailId.indexOf("@");
  dotpos = emailId.lastIndexOf(".");
  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailId.length) {
      alert("Not a valid e-mail address");
  }
  else{
    return true;
  }
}
function passValidate() {
  var password = document.getElementById('password').value;
  var confirmPassword = document.getElementById('confirmPassword').value;
  if(password=="" || password.length<=7){
    alert("Password cannot be empty and minimum length 8");
  }else if (confirmPassword=="" || confirmPassword.length<=7) {
    alert("Confirm Password cannot be empty and minimum length 8");
  }else if (password!=confirmPassword) {
    alert("Password doesn't match");
  }else {
    return true;
  }
}
function checkboxValidate(){
  var checkBox = document.getElementById('agreeTerms').checked;
  if(checkBox==false){
    alert("Agree to terms and conditions");
  }else{
    return true;
  }
}
